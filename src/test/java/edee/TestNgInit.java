package edee;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import edee.commons.ApplicationContext;
import edee.commons.driver.ChromeDriverServerConfig;

/**
 * Skeleton class for  TestNG tests
 * @author Michal Travnicek
 */
public class TestNgInit {

	protected ApplicationContext context;

	public ApplicationContext getContext() {
		return context;
	}

	public TestNgInit() {
		context = new ApplicationContext();
	}

	@BeforeClass
	public void setUp() {
		System.out.println("SETUP FOR TEST");
		context.initializeWebDriver(new ChromeDriverServerConfig());
	}

	@BeforeTest
	public void beforeEachTest() {

	}

	@AfterTest
	public void afterEachTest() {

	}

	/**
	 * After end of testsuite shutdown driver.
	 * Runs always even when skipping invocations due to failures.
	 */
	@AfterClass(alwaysRun=true)
	public void afterClass() {
		context.getDriver().quit();
	}

}
