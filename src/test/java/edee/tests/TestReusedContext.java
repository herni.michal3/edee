package edee.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import edee.commons.ApplicationContext;
import edee.commons.driver.ChromeDriverServerConfig;
import edee.pages.login.LoginPage;

/**
 * Reused single context is fastest but prone to intermittent failures when used in consecutive tests.
 * Note: Can give error "Too many login attempts" due to too many successive logins.
 * 
 * @author Michal T
 *
 */
public class TestReusedContext {

    @BeforeClass
    public void init() throws InstantiationException, IllegalAccessException {
        timer = System.currentTimeMillis();
        reusedContext = new ApplicationContext(driverConfig.newInstance());
    }

    @AfterClass
    public void close() {
        timer = System.currentTimeMillis() - timer;
        System.out.println("REAL TEST DURATION:" + timer / 1000 + " seconds");
    }

    @BeforeMethod
    public void beforeMethod() {
    }

    @AfterMethod
    public void afterMethod() {
    	reusedContext.getNewSession();
    }

    @AfterTest(alwaysRun=true)
    public void afterTest() {
        System.out.println("After test");
        reusedContext.quitWebDriver();
    }

    Class<ChromeDriverServerConfig> driverConfig = ChromeDriverServerConfig.class;
    ApplicationContext reusedContext;
    Long timer;

    /**
     * Test that Login is successful with login and password "demo","demo".
     * Artificial wait is useful, because login too fast in succession generates Too many login attempts = hammering protection.
     *
     * @throws Exception
     */
    @Test(threadPoolSize = 1, invocationCount = 8, skipFailedInvocations= true)
    public void testLoginSuccess3() throws Exception {
        System.out.println(reusedContext);
        //Thread.sleep(2500);
        LoginPage loginPage = new LoginPage.Builder(reusedContext).load().build();
        loginPage.submitLogin("demo", "demo");
        loginPage.check().checkLoginSuccess(reusedContext);
    }


}
