package edee.tests;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import edee.commons.ApplicationContext;
import edee.commons.driver.ChromeDriverServerConfig;
import edee.pages.login.LoginPage;

/**
 * Thread safe test with new driver instance for each test invocation.
 * Seriously slower than reused instance, but that can be compensated by running in 
 * more threads simultaneously.
 * Note: Can give error "Too many login attempts" due to too many successive logins. 
 * @author Michal T
 *
 */
public class TestParallel {

    @BeforeClass
    public void init() throws InstantiationException, IllegalAccessException {
        timer = System.currentTimeMillis();
    }

    @AfterClass
    public void close() {

        timer = System.currentTimeMillis() - timer;
        System.out.println("REAL TEST DURATION:" + timer / 1000 + " seconds");

    }

    @BeforeMethod
    public void beforeMethod() throws InstantiationException, IllegalAccessException {
    
        if (contextThreaded.get() == null) {
            contextThreaded.set(new ApplicationContext(driverConfig.newInstance()));
            contextInstances.add(contextThreaded.get());
        } else {
           contextThreaded.get().setDriver(driverConfig.newInstance());
           
        }
    }

    @AfterMethod
    public void afterMethod() {
    	contextThreaded.get().quitWebDriver();
    	//context.get().getDriver().close();

    }

    @AfterTest
    public void afterTest() {
        System.out.println("After test");

        for (ApplicationContext i : contextInstances) {
            try {
                System.out.println(i);
            } catch (Exception x) {
            }
        }
    }

    Class<ChromeDriverServerConfig> driverConfig = ChromeDriverServerConfig.class;
    ThreadLocal<ApplicationContext> contextThreaded = new ThreadLocal<>();    
    List<ApplicationContext> contextInstances = new ArrayList<>();
    Long timer;

    /**
     * Test that Login is successful with login and password "demo","demo".
     * Artificial wait is useful, because login too fast in succession generates Too many login attempts = hammering protection.
     *
     * @throws Exception
     */
    @Test(threadPoolSize = 4, invocationCount = 8, skipFailedInvocations= true)
    public void testLoginSuccessMultithreaded() throws Exception {
        System.out.println(contextThreaded.get());
        //Thread.sleep(2500);
        LoginPage loginPage = new LoginPage.Builder(contextThreaded.get()).load().build();
        loginPage.submitLogin("demo", "demo");
        loginPage.check().checkLoginSuccess(contextThreaded.get());
    }
    

}
