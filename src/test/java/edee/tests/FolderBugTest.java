package edee.tests;

import org.testng.annotations.Test;

import edee.TestNgInit;
import edee.pages.filespanel.FileBrowser;
import edee.pages.login.LoginPage;


/**
 * This test demonstrates bug when going into subfolder and up
 * and then creating (or deleting etc.) folder on root level that
 * instead folder gets created (or performed some other operation) on subfolder level.
 * @author Michal T
 *
 */
public class FolderBugTest extends TestNgInit {

	private FileBrowser fileBrowser;
	
	/**
	 * Folder name must be lowercase.
	 */
	private final String TEST_FOLDER = "bug";
	private final String TEST_FOLDER2 = "some";
	

	@Test(dataProvider = "demoLogin", dataProviderClass = TestData.class)
	public void loadFileBrowser(String login, String password) throws Exception {
		LoginPage loginPage = new LoginPage(context);
		loginPage.submitLogin(login, password).check().checkLoginSuccess(context);
		fileBrowser = new FileBrowser.Builder(context).load().waitToLoad().build();
	}
	
	@Test (dependsOnMethods= {"loadFileBrowser"}) 
	public void createFolder() throws Exception{
		fileBrowser.createFolderAndWait(TEST_FOLDER);
		//Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= {"createFolder"}) 
	public void goDown() throws Exception{
		fileBrowser.openFolderAndWait(TEST_FOLDER);
 		Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= {"goDown"}) 
	public void goUp() throws Exception{
		fileBrowser.goToParentFolder();
		Thread.sleep(2000);
	}
	
	/**
	 * This test should throw assertion error because directory gets created in subdirectory of current directory.
	 * @throws Exception
	 */
	@Test (dependsOnMethods= {"goUp"}) 
	public void createAnotherFolder() throws Exception{
		fileBrowser.createFolderAndWait(TEST_FOLDER2);
	}
	 

}
