package edee.tests;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import edee.TestNgInit;
import edee.pages.filespanel.FileBrowser;
import edee.pages.login.LoginPage;


public class FolderTest extends TestNgInit {

	private FileBrowser fileBrowser;
	
	/**
	 * Folder name must be lowercase.
	 */
	private final String TEMP_FOLDER = "name";

	@Test(dataProvider = "demoLogin", dataProviderClass = TestData.class)
	public void loadFileBrowser(String login, String password) throws Exception {
		LoginPage loginPage = new LoginPage(context);
		loginPage.submitLogin(login, password).check().checkLoginSuccess(context);
		fileBrowser = new FileBrowser.Builder(context).load().waitToLoad().build();
	}
	
	@Test (dependsOnMethods= { "loadFileBrowser"}) 
	public void createFolder() throws Exception{
		fileBrowser.createFolderAndWait(TEMP_FOLDER);
		//Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= { "createFolder"}) 
	public void selectFolder() throws Exception{		
		WebElement element = fileBrowser.getItemCheckbox(TEMP_FOLDER); 
		element.click();
		assertTrue(element.isSelected());		
		Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= { "selectFolder"}) 
	public void unselectFolder() throws Exception{
		WebElement element = fileBrowser.getItemCheckbox(TEMP_FOLDER);
		element.click();
		assertFalse(element.isSelected());
		Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= { "unselectFolder"}) 
	public void openFolder() throws Exception{
		fileBrowser.openFolderAndWait(TEMP_FOLDER);
		Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= { "openFolder"}) 
	public void goToParentFolder() throws Exception{
		fileBrowser.goToParentFolder();
		Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= { "goToParentFolder"}) 
	public void deleteFolder() throws Exception{
		fileBrowser.getItemCheckbox(TEMP_FOLDER).click();
		fileBrowser.getDeleteButton().click();
		fileBrowser.new DeletePopup().getSubmit().click();
		Thread.sleep(2000);
	}
	
	/**
	 * Does not check confirmation message about deleting - TODO
	 * @throws Exception
	 */
	@Test (expectedExceptions = NoSuchElementException.class, dependsOnMethods= { "deleteFolder"}) 
	public void verifyDeleted() throws Exception{
		fileBrowser.getItemCheckbox(TEMP_FOLDER);
	}
	
	
	
	 

}
