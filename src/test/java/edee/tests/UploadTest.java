package edee.tests;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import edee.TestNgInit;
import edee.pages.filespanel.FileBrowser;
import edee.pages.login.LoginPage;
import edee.workers.UploadRobot;

@Test (groups = {"upload"}, dataProviderClass = TestData.class)
public class UploadTest extends TestNgInit {

	private FileBrowser fileBrowser;
	private String filePath;
	private String uploadFolder;
	private String file;
	
	@BeforeClass
	@Test(dataProvider = "uploadTest", dataProviderClass = TestData.class)
	public void provider(String folderName, String uploadedFile, String path) {
		uploadFolder = folderName;
		file = uploadedFile; 
		filePath = path;
	}
	
	@Test(dataProvider = "demoLogin")
	public void loadFileBrowser(String login, String password) throws Exception {
		LoginPage loginPage = new LoginPage(context);
		loginPage.submitLogin(login, password).check().checkLoginSuccess(context);
		fileBrowser = new FileBrowser.Builder(context).load().waitToLoad().build();
	}
	
	@Test (dependsOnMethods= {"loadFileBrowser"}) 
	public void createFolder() throws Exception{
		fileBrowser.createFolderAndWait(uploadFolder);
		//Thread.sleep(2000);
	}
	
	/**
	 * Some waits like here are mandatory for subfolder fully load (no other reliable method yet)
	 * @throws Exception
	 */
	@Test (dependsOnMethods= {"createFolder"}) 
	public void goDown() throws Exception{
		fileBrowser.openFolderAndWait(uploadFolder);
 		Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= {"goDown"}) 
	public void clickUpload() throws Exception{
		fileBrowser.getUploadButton().click();
		Thread.sleep(2000);
	}
	
	@Test (dependsOnMethods= {"clickUpload"}) 
	public void uploadFileWithRobot() throws Exception{
		new UploadRobot().setFilePath(filePath).execute(context);
	}
	
	/**
	 * Test should be stricter, simpler and check directly on file name...TODO
	 * @throws Exception
	 */
	@Test (groups = {"uploadFinished"},dependsOnMethods= {"uploadFileWithRobot"}) 
	public void confirmUploadSuccess() throws Exception{
		
		List<WebElement> elements =
				fileBrowser.getFileTableRoot().findElements(By.cssSelector("input:checked[type='checkbox']"));
		System.out.println(elements.size());
		assertTrue(elements.size()==1 || fileBrowser.getItemCheckbox(file).isSelected());
		

	}

}
