package edee.tests;

import java.io.File;

import org.testng.annotations.DataProvider;

import edee.commons.AppProperties;

public class TestData {
	

	private static final String FILE = AppProperties.get(AppProperties.UPLOAD_FILE);
	private static final String FILE_PATH = AppProperties.CLASSPATH + File.separator + FILE;
	
	@DataProvider(name = "demoLogin")
	  public static Object[][] demoLogin() {
	    return new Object[][] {
	      new String[] {"demo","demo"}
	    };
	  }
	
	/**
	 * Folder name for upload, uploaded file name, path to uploaded file.
	 * 
	 * Folder name and file name must be/will be lowercase.
	 */
	@DataProvider(name = "uploadTest")
	  public static Object[][] uploadFolderName() {
	    return new Object[][]{
	    	 new String[] {"upload", FILE.toLowerCase(), FILE_PATH}
	    };
	  }
	

}
