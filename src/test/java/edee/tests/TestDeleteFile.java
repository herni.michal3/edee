package edee.tests;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import edee.TestNgInit;
import edee.pages.filespanel.FileBrowser;
import edee.pages.login.LoginPage;

/**
 * Depends on test UploadTest...best to run from UploadAndDelete.xml as TestNG
 * suite test
 * 
 * @author Michal T
 *
 */
public class TestDeleteFile extends TestNgInit {

	@BeforeGroups(groups = "upload")
	public void setup() {
		System.out.println("Starting upload scenario");
	}

	private FileBrowser fileBrowser;
	private String uploadFolder;
	private String file;

	@Test(dataProvider = "uploadTest", dataProviderClass = TestData.class)
	@BeforeClass
	public void provider(String folderName, String uploadedFile, String path) {
		uploadFolder = folderName;
		file = uploadedFile;
	}

	@Test(groups = { "some" }, dataProvider = "demoLogin", dataProviderClass = TestData.class)
	public void loadFileBrowser(String login, String password) throws Exception {
		LoginPage loginPage = new LoginPage(context);
		loginPage.submitLogin(login, password).check().checkLoginSuccess(context);
		fileBrowser = new FileBrowser.Builder(context).load().waitToLoad().build();
	}

	@Test(dependsOnMethods = { "loadFileBrowser" })
	public void goDown() throws Exception {
		fileBrowser.openFolderAndWait(uploadFolder);
		Thread.sleep(2000);
	}

	/**
	 * Select all files containing uploaded file name.
	 * fileBrowser.getItemCheckbox(file).click(); works on exact match
	 * 
	 * @throws Exception
	 */
	@Test(dependsOnMethods = { "goDown" }, skipFailedInvocations = true)
	public void selectFile() throws Exception {

		String partial = file.substring(0, file.lastIndexOf("."));
		List<WebElement> elements = fileBrowser.getFilesContaining(partial);
		elements.forEach(WebElement::click);
		Thread.sleep(1000);

	}

	@Test(dependsOnMethods = { "selectFile" })
	public void deleteFile() throws Exception {
		fileBrowser.getDeleteButton().click();
		fileBrowser.new DeletePopup().getSubmit().click();
		Thread.sleep(2000);
	}

	/**
	 * Check no files partly matching name exist. fileBrowser.getItemCheckbox(file);
	 * works only on exact match of filename
	 * 
	 * @throws Exception
	 */
	@Test( dependsOnMethods = { "deleteFile" })
	public void confirmDeleted() throws Exception {
		String partial = file.substring(0, file.lastIndexOf("."));
		List<WebElement> elements = fileBrowser.getFilesContaining(partial);
		assertTrue(elements.size()==0);
	}

}
