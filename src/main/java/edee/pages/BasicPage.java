package edee.pages;

import java.lang.reflect.Field;

import org.openqa.selenium.WebElement;

import edee.commons.ApplicationContext;
import edee.commons.utils.ElementWrapper;
import edee.commons.utils.PageUtils;

/**
 * Template for large pages. Introduces PageURL string.
 * 
 * @param load
 *            - should the page load itself using "loadURL"?
 * @param wait
 *            - should the page wait until it is loaded using "waitUntilLoaded"?
 * @param screenShot
 *            - create screenshot of page?
 * @author Michal T
 *
 */
public abstract class BasicPage extends ProxyPage {

	public BasicPage(ApplicationContext context, boolean load, boolean wait, boolean screenShot) throws Exception {
		super(context);
		this.context = context;

		if (load) {
			loadURL(context);
		}
		if (wait) {
			waitUntilLoaded(context);
		}
		if (screenShot) {
			takeScreenshot(context);
		}

	}

	/**
	 * Application context for retrieving webdriver instance etc. 
	 */
	private ApplicationContext context;

	private final static String VALID_URL = "http.*:/.*";

	/**
	 * Stored pageURL that can be loaded or compared to actual URL.
	 */
	@FindByName
	protected String pageURL;

	protected ApplicationContext getContext() {
		return context;
	}

	/**
	 * Method for confirming page is loaded - unreliable.
	 * 
	 * @param context
	 * @throws Exception
	 */
	public void waitForURL(ApplicationContext context) throws Exception {
		context.invokeWait().waitForURLMatch(PageUtils.patternizeURL(pageURL));
	}

	/**
	 * Loads page using its preset URL from datasource.
	 * @param context
	 */
	public void loadURL(ApplicationContext context) {

		System.out.println("LOADING URL:" + pageURL);
		if (pageURL.matches(VALID_URL)) {
			context.getDriver().get(pageURL);
		} else {
			throw new RuntimeException("Malformed URL:" + getPageId(getClass()) + ";" + pageURL);
		}

	}

	/**
	 * Recursively scans through page hierarchy, until it finds WebElement that can
	 * be used to verify, that this page is loaded/displayed.
	 *
	 * @param context
	 * @return
	 * @throws Exception
	 */
	public boolean waitUntilLoaded(ApplicationContext context) throws Exception {
		
		System.out.println("WAITING FOR PAGE TO LOAD");
		Class<?> pageClass = getClass();

		while (!pageClass.equals(BasicPage.class)) {
			if (waitUntilLoaded(context, pageClass)) {
				return true;
			}
			pageClass = pageClass.getSuperclass();
		}

		throw new RuntimeException("Page not loaded - no element found:" + getClass());

	}

	/**
	 * Finds first defined WebElement on page and waits for it to appear in browser
	 *
	 * @param context
	 * @param pageClass
	 * @return
	 * @throws Exception
	 */
	public boolean waitUntilLoaded(ApplicationContext context, Class<?> pageClass) throws Exception {

		try {
			for (Field field : pageClass.getDeclaredFields()) {
				if (field.isAnnotationPresent(FindByName.class) && field.getType().equals(WebElement.class)
						&& field.getAnnotation(FindByName.class).visibleOnload()) {
					field.setAccessible(true);
					WebElement element = (WebElement) field.get(this);
					context.invokeWait().waitForElementToBeClickable(element);
					return true;
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return false;
	}

	/**
	 * Wraps element using ElementWrapper for safety.
	 * 
	 * @param element
	 * @return
	 */
	protected WebElement wrapElement(WebElement element) {
		return new ElementWrapper(element).getElement(getContext());
	}

	/**
	 * Returns URL assigned from datasource.
	 * 
	 */
	public String getAssignedURL() {
		return pageURL;
	}

	/**
	 * Builder used for creating pages, can be extended.
	 *
	 * @param <P>
	 */
	public abstract static class PageBuilder<P extends BasicPage> {

		ApplicationContext context;
		boolean load = false;
		boolean wait = false;
		boolean screenShot = false;

		public PageBuilder(ApplicationContext context) {
			this.context = context;
		}

		/**
		 * Load page URL.
		 *
		 * @return
		 */
		public PageBuilder<P> load() {
			load = true;
			return this;
		}

		/**
		 * Wait until page elements loaded.
		 *
		 * @return
		 */
		public PageBuilder<P> waitToLoad() {
			wait = true;
			return this;
		}

		/**
		 * Take screenshot on page init.
		 *
		 * @return
		 */
		public PageBuilder<P> screenShot() {
			screenShot = true;
			return this;
		}

		/**
		 * Commits build.
		 *
		 * @return
		 * @throws Exception
		 */
		public P build() throws Exception {
			return runBuilder(context, load, wait, screenShot);
		}

		/**
		 * Override method for page constructor run by builder.
		 */
		protected abstract P runBuilder(ApplicationContext context, boolean load, boolean wait, boolean screenShot)
				throws Exception;
	}
}
