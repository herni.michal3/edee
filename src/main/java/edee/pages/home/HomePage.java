package edee.pages.home;

import org.openqa.selenium.WebElement;

import edee.commons.ApplicationContext;
import edee.pages.BasicPage;

public class HomePage extends BasicPage {
	
//	public static class Builder extends PageBuilder<HomePage> {
//
//		public Builder(ApplicationContext context) {
//			super(context);
//		}
//
//		@Override
//		protected HomePage runBuilder(String id, ApplicationContext context, boolean load, boolean wait,
//				boolean screenShot) throws Exception {
//			return new HomePage(id,context, load, wait, screenShot);
//		}
//	}
	
	//!!!!homepage is compound of multiple pages!!!
	// and we dont always need to load side panels, only buttons
	
	/**
	 * @param context
	 * @param wait
	 * @throws Exception
	 */
	public HomePage(ApplicationContext context, boolean wait) throws Exception {
		super(context, false, wait, true);	
	}	
	
	public HomePage(ApplicationContext context, boolean load, boolean wait, boolean screenShot) throws Exception {
		super(context, load, wait, screenShot);
	}

	@FindByName
    private WebElement contentMenuClickButton;
	@FindByName
    private WebElement filesMenuClickButton;
	@FindByName
    private WebElement actionsMenuHoverAndClickButton;

    public WebElement getContentMenuButton() {
        return contentMenuClickButton;
    }

    public WebElement getFilesMenuClickButton() {
        return filesMenuClickButton;
    }
	

}
