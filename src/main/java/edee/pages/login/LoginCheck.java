package edee.pages.login;

import edee.commons.ApplicationContext;
import edee.commons.waits.CustomExpectedConditions;

public class LoginCheck {

    private LoginPage page;

    public LoginCheck(LoginPage page) {    	
        this.page = page;
    }

    /**
     * Check that after successful login URL changes to homepage pattern.
     * 
     * @param context
     * @return
     * @throws Exception
     */
    public boolean checkLoginSuccess(ApplicationContext context) throws Exception {
        
        int TIMEOUT = 5;
        
        boolean succes = context.invokeWait().waitFor(CustomExpectedConditions.pageTitleisNot(page.getPageTitle()));
        context.invokeWait(TIMEOUT).waitForURLMatch(page.entryURLpattern);
        return succes;
    }

    /**
     * Check that unsuccessful login generates various error messages.
     * @param context
     * @return
     */
    public boolean checkLoginFailMessages(ApplicationContext context) {
        int TIMEOUT = 15;
        int INTERVAL = 1000;

        return context.invokeWait(TIMEOUT, INTERVAL).waitFor(
                CustomExpectedConditions.textMatchesPattern(page.getStatusContainer(), page.loginFailed),
                CustomExpectedConditions.textMatchesPattern(page.getStatusContainer(), page.tooManyAttempts),
                CustomExpectedConditions.textMatchesPattern(page.getStatusContainer(), page.accountLocked));

    }

}
