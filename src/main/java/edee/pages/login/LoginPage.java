/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edee.pages.login;

import org.openqa.selenium.WebElement;

import edee.commons.ApplicationContext;
import edee.pages.BasicPage;
import edee.pages.ProxyPage.PageId;
import edee.strings.model.MultiString;

/**
 * Login page is first page displayed with login prompt. Page id is overrided to
 * "login". This value is searched in datasource.
 *
 *
 * @author Michal Travnicek
 */
@PageId(id = "login")
public class LoginPage extends BasicPage {

	public static class Builder extends PageBuilder<LoginPage> {

		public Builder(ApplicationContext context) {
			super(context);
		}

		@Override
		protected LoginPage runBuilder(ApplicationContext context, boolean load, boolean wait, boolean screenShot)
				throws Exception {
			return new LoginPage(context, load, wait, screenShot);
		}
	}

	/**
	 * Default LoginPage constructor which loads the page
	 * 
	 * @param context
	 * @throws Exception
	 */
	public LoginPage(ApplicationContext context) throws Exception {
		this(context, true, true, true);
	}

	/**
	 * Alternative constructor allowing to not load page
	 * 
	 * @param context
	 * @param load
	 *            - load page yes/no
	 * @throws Exception
	 */
	public LoginPage(ApplicationContext context, boolean load) throws Exception {
		this(context, load, true, true);
	}

	public LoginPage(ApplicationContext context, boolean load, boolean wait, boolean screenShot) throws Exception {
		super(context, load, wait, screenShot);
	}

	/**
	 * Localized title of page that is stored in datasource. Can be compared to
	 * driver.getTitle();
	 */
	@FindByName
	private MultiString pageTitle;

	/**
	 * Field for entering name.
	 */
	@FindByName
	private WebElement nameBox;

	/**
	 * Field for entering password.
	 */
	@FindByName
	private WebElement passwordBox;
	
	/**
	 * Button for submitting login form.
	 */
	@FindByName
	private WebElement submitButton;
	
	/**
	 * Status bar at the bottom of login form displaying various status messages.
	 */
	@FindByName
	private WebElement statusContainer;
	
	/**
	 * Button with British flag
	 */
	@FindByName
	private WebElement enButton;
	/**
	 * Button with Czech flag
	 */
	@FindByName
	private WebElement csButton;

	/**
	 * Czech or British button which is currently active/selected.
	 */
	@FindByName
	private WebElement activeButton;

	/**
	 * Various multilingual messages show in status bar
	 * 
	 */
	@FindByName
	protected MultiString encrypting;
	@FindByName
	protected MultiString loginFailed;
	@FindByName
	protected MultiString tooManyAttempts;
	@FindByName
	protected MultiString accountLocked;

	/**
	 * URL of entry page (page that opens on successful login) = homepage.
	 */
	@FindByName
	protected String entryURLpattern;

	public MultiString getPageTitle() {
		return pageTitle;
	}

	public WebElement getNameBox() {
		return nameBox;
	}

	public WebElement getPasswordBox() {
		return passwordBox;
	}

	public WebElement getSubmitButton() {
		return submitButton;
	}

	public WebElement getStatusContainer() {
		return statusContainer;
	}

	public WebElement getEnButton() {
		return enButton;
	}

	public WebElement getCsButton() {
		return csButton;
	}

	/**
	 * Returns cs or en button which is currently selected (active)
	 * 
	 * @return
	 */
	public WebElement getActiveLocButton() {
		return activeButton;
	}

	public void enterUserLogin(String login) {
		nameBox.sendKeys(login);
	}

	public void enterUserPassword(String password) {
		passwordBox.sendKeys(password);
	}

	public void submitLoginForm() {
		submitButton.click();
	}

	/**
	 * Enters credentials into login form and submits
	 * 
	 * @param login
	 * @param password
	 */
	public LoginPage submitLogin(String login, String password) {
		enterUserLogin(login);
		enterUserPassword(password);
		submitLoginForm();
		return this;
	}

	public LoginCheck check() {
		return new LoginCheck(this);
	}

}
