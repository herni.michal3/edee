package edee.pages.filespanel;

import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import edee.commons.ApplicationContext;
import edee.commons.utils.ElementUtils;
import edee.commons.utils.ExceptionHandler;
import edee.commons.utils.PageUtils;
import edee.commons.waits.CustomExpectedConditions;
import edee.pages.BasicPage;
import edee.pages.Modal;
import edee.strings.model.MultiString;

/**
 * Files panel containing files and folders view and buttons: New directory,
 * Rename, Properties, Copy, Delete
 * 
 * 
 * @author Michal Travnicek
 *
 */
public class FileBrowser extends BasicPage {

	/**
	 * Dialog for creating directory.
	 * Contains field for name and buttons Create and Storno.
	 *
	 */
	public class CreateDir extends Modal {

		public CreateDir() throws Exception {
			super(getContext(), true);
		}

		public CreateDir submitCreateDirDialog(String dirName) {
			getDirName().click();
			getDirName().sendKeys(dirName);
			getSubmit().click();
			return this;
		}

		/**
		 * Waits/checks until directory was succesfully created.
		 * 
		 * @param context
		 * @throws Exception
		 */
		public void checkCreated(String dirName) throws Exception {

			Logger.getLogger(getClass().getName()).log(Level.INFO, "Success alert:" + getAlertSuccess().getText());
			getContext().invokeWait()
					.waitFor(CustomExpectedConditions.textMatchesPattern(alertSuccess, dirCreatedConfirmation));
			String folderPath = PageUtils.extractPattern(alertExtractPattern, alertSuccess.getText());
			assertEquals(folderPath, "/"+dirName);

		}

		/**
		 * Input box for directory name
		 */
		@FindByName
		private WebElement dirName;

//		/**
//		 * Text of confirmation when was directory successfully created.
//		 */
//		@FindByName
//		private MultiString successConfirmation;


		public WebElement getDirName() {
			return dirName;
		}

		public WebElement getAlertSuccess() {
			return alertSuccess;
		}
	}

	public class DeletePopup extends Modal {

		public DeletePopup() throws Exception {
			super(getContext(), true);
		}

	}

	/**
	 * New FilesPanel instance
	 * 
	 * @param context
	 * @param wait
	 *            - should the page load (wait for loaded)
	 * @throws Exception
	 */
	public FileBrowser(ApplicationContext context, boolean wait) throws Exception {
		super(context, false, wait, true);
	}

	public FileBrowser(ApplicationContext context, boolean load, boolean wait) throws Exception {
		super(context, load, wait, true);
	}

	public FileBrowser(ApplicationContext context, boolean load, boolean wait, boolean screenShot) throws Exception {
		super(context, load, wait, true);
	}

	public static class Builder extends PageBuilder<FileBrowser> {

		public Builder(ApplicationContext context) {
			super(context);
		}

		@Override
		protected FileBrowser runBuilder(ApplicationContext context, boolean load, boolean wait, boolean screenShot)
				throws Exception {
			return new FileBrowser(context, load, wait, screenShot);
		}

	}

	/**
	 * Button called "Upload" for uploading files.
	 */
	@FindByName(visibleOnload = false)
	private WebElement uploadButton;

	/**
	 * Button called "New directory" for creating directory.
	 */
	@FindByName
	private WebElement newDirectoryButton;

	/**
	 * Button called "Delete" for deleting files.
	 */
	@FindByName(visibleOnload = false)
	private WebElement deleteButton;

	/**
	 * Panel displaying current location in files tree
	 */
	@FindByName
	private WebElement filesPath;
	
	/**
	 * Element displaying successful operation with directory or file.
	 */
	@FindByName(visibleOnload = false)
	private WebElement alertSuccess;
	
	/**
	 * Text of confirmation when directory is successfully created.
	 */
	@FindByName
	private MultiString dirCreatedConfirmation;

	/**
	 * Root of table with list of folders and files.
	 */
	@FindByName
	private WebElement fileTableRoot;
	
	/**
	 * Text of link to parent folder.
	 */
	@FindByName
	private MultiString parentFolder;
	
	/**
	 * Pattern to extract locale from URL.
	 */
	@FindByName
	private String localeURLpattern;
	
	/**
	 * Pattern to extract folder or file name from creation/deletion confirmation message.
	 */
	@FindByName
	private String alertExtractPattern;
	
	
	
	/**
	 * Detects page locale relative to page fragment url.
	 * @return
	 */
	public String getPageLocale() {
		return PageUtils.extractPattern(localeURLpattern, getContext().getDriver().getCurrentUrl());
	}
	
	/**
	 * Goes to parent folder and verifies path changed.
	 * @throws Exception
	 */
	public void goToParentFolder() throws Exception {
		String initialPath = getFilesPath().getText();
		String linkText = getContext().strings().getLocaleString(parentFolder, getPageLocale());
		WebElement element = getFileTableRoot().findElement(By.linkText(linkText));
		wrapElement(element).click();
		getContext().invokeWait().waitForElementText(getFilesPath(),
				initialPath.substring(0, initialPath.lastIndexOf(PATH_SEPARATOR)));
	}
	

	private final String PATH_SEPARATOR = " > ";

	/**
	 * Gets HREF link element of directory item.
	 * @param name
	 * @return
	 */
	public WebElement getFolderLink(String name) {
		WebElement directoryLink = null;
		try {
			directoryLink = getFileTableRoot().findElement(By.linkText(name));
		} catch (Exception e) {
			ExceptionHandler.notFound(getClass(), e, "Directory \"" + name + "\"");
		}
		return wrapElement(directoryLink);
	}

	/**
	 * Opens directory with confirmation, waits for path indicator to show new path.
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public FileBrowser openFolderAndWait(String name) throws Exception {
		String initialPath = getFilesPath().getText();
		getFolderLink(name).click();
		getContext().invokeWait().waitForElementText(getFilesPath(), initialPath + PATH_SEPARATOR + name);
		return this;
	}

	/**
	 * Creates directory in file list and waits until it is created and confirmed.
	 * 
	 * @param dirName
	 * @return
	 * @throws Exception
	 */
	public FileBrowser createFolderAndWait(String dirName) throws Exception {
		getNewDirectoryButton().click();
		new CreateDir().submitCreateDirDialog(dirName).checkCreated(dirName);
		return this;
	}
	
	/**
	 * Gets item - file or folder in file list.
	 * @param name
	 * @return
	 */
	public WebElement getItemCheckbox (String name) {
		return  getFileTableRoot().findElement(By.cssSelector("input[data-item*='{\"name\": \"" + name + "\"']"));
	}
	
	/**
	 * Returns files elements containing specific text, if is element clicked it will select file.
	 * @param name
	 * @return
	 */
	public List<WebElement> getFilesContaining (String name) {
		List<WebElement> files = null;
		try {
			files = new ElementUtils(getFileTableRoot()).getElementsWithText(name);
		} catch (Exception e) {
			ExceptionHandler.notFound(getClass(), e, "Files with \"" + name + "\"");
		}

		return files;
	}
	
	

	public WebElement getDeleteButton() {
		return deleteButton;
	}

	public WebElement getUploadButton() {
		return uploadButton;
	}

	/**
	 * Panel displaying current location in files tree
	 */
	public WebElement getFilesPath() {
		return filesPath;
	}

	/**
	 * Panel displaying current location in files tree
	 */
	public WebElement getFileTableRoot() {
		return fileTableRoot;
	}

	public WebElement getNewDirectoryButton() {
		return newDirectoryButton;
	}

}
