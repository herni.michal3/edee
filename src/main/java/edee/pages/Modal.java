package edee.pages;

import org.openqa.selenium.WebElement;

import edee.commons.ApplicationContext;

public class Modal extends ProxyPage {

	/**
	 * Modal dialog with buttons Yes/No represented by btn1,btn2 elements.
	 * 
	 * @param context
	 * @param screenShot
	 * @throws Exception
	 */
	public Modal(ApplicationContext context, boolean screenShot) throws Exception {
		super(context);
		if (screenShot) {
			takeScreenshot(context);
		}
	}

	/**
	 * Submit button
	 */
	@FindByName
	private WebElement submit;

	/**
	 * Cancel button
	 */
	@FindByName
	private WebElement cancel;

	/**
	 * @return confirm button = Ok, Submit, Create etc.
	 */
	public WebElement getSubmit() {
		return submit;
	}

	/**
	 * @return cancel button = No, Cancel etc.
	 */
	public WebElement getCancel() {
		return cancel;
	}

}
