package edee.pages;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import edee.commons.ApplicationContext;
import edee.commons.utils.ScreenshotUtil;
import edee.strings.model.MultiString;

/**
 *
 * @author Michal Travnicek
 */
public abstract class ProxyPage {

	/**
	 * Initializes page, assigns fields from datasource using annotation FindByName.
	 * Assigns string and multistring and WebElements from datasource under specific
	 * "pageId". WebElements are wrapped in local proxy for avoiding Stale Element
	 * Reference exception.
	 *
	 * @param context
	 *            - WebDriver context
	 * 
	 * @throws Exception
	 */
	public ProxyPage(ApplicationContext context) throws Exception {

		Class<?> pageClass = getClass();

		while (!pageClass.equals(Object.class)) {
			assignByReflection(pageClass, getPageId(getClass()), context);
			pageClass = pageClass.getSuperclass();
		}
	}

	protected String createIdFromClass(Class<?> pageClass) {
		return pageClass.getSimpleName().toLowerCase();
	}

	/**
	 * Recursively scans FindByName annotation in page hierarchy and assigns WebElements, String and Multistring
	 * fields from datasource based on name of field. WebElements can be inherited from upper classes,
	 * for example CreateDir modal inherits buttons from Modal.
	 * 
	 * @param pageClass
	 * @param pageId
	 * @param context
	 * @throws Exception
	 */
	protected void assignByReflection(Class<?> pageClass, String pageId, ApplicationContext context) throws Exception {
		for (Field field : pageClass.getDeclaredFields()) {
			if (field.isAnnotationPresent(FindByName.class)) {

				String fieldName = field.getName();
				field.setAccessible(true);
				String id;
				String fieldClassId = getPageId(field.getDeclaringClass());

				if (!fieldClassId.equals(pageId)) {
					id = fieldClassId;
				} else {
					id = pageId;
				}

				if (field.getType().equals(String.class)) {
					field.set(this, context.strings().getStringValue(pageId, fieldName));

				} else if (field.getType().equals(MultiString.class)) {
					field.set(this, context.strings().getMultiStringValue(pageId, fieldName));
				} else if (field.getType().equals(WebElement.class)) {
					Logger.getLogger(getClass().getName()).log(Level.FINE, field.toString());
					By bye = context.strings().getByFromString(id, fieldName);
					field.set(this, getElement(context, bye));
				}
			}
		}

	}

	/**
	 * Get page id based on either PageId annotation or class name.
	 * @param pageClass
	 * @return
	 */
	protected String getPageId(Class<?> pageClass) {
		if (pageClass.isAnnotationPresent(PageId.class)) {
			return pageClass.getAnnotation(PageId.class).id();
		} else {
			return createIdFromClass(pageClass);
		}

	}

	/**
	 * This is proxy for WebElement, String and MultiString fetched from database of
	 * strings. WebElement is looked up on use to avoid Stale Element Reference
	 * Exception. This is similar behavior to how FindBy annotation works with
	 * PageObject/PageFactory pattern in Selenium.
	 *
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public @interface FindByName {
		/**
		 * Is element visible, when page is loaded? Default = yes
		 */
		boolean visibleOnload() default true;
	}

	/**
	 * Overrides descriptor of page "pageId" used to read data from datasource.
	 * Otherwise it is derived from class name.
	 * 
	 * @author Michal
	 *
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	public @interface PageId {
		/**
		 * Page ID override
		 */
		String id();
	}

	 /**
	 * Creates screenshot in preset location in format pageName + current time
	 * @param context
	 * @throws Exception
	 */
	 public void takeScreenshot(ApplicationContext context) throws Exception {
		 ScreenshotUtil.screenshotToSetFolder(context.getDriver(), getClass().getSimpleName()+ System.currentTimeMillis());	        
	 }

	/**
	 * In contrast to "webDriver.findElement(by)" which creates instance of
	 * WebElement rightaway, this proxy initializes element lazily when used like
	 * FindBy pagefactory proxy in Selenium. This allows us to prevent Stale Element
	 * exceptions and debug logging.
	 *
	 */
	protected final WebElement getElement(ApplicationContext context, By by) {
		return (WebElement) Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[] { WebElement.class },
				new InvocationHandler() {

					/**
					 * Lazily initialized instance of WebElement
					 */
					private WebElement webElement;

					@Override
					public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

						if (method.toString().matches(".*WebElement.*")) {
							Logger.getLogger(getClass().getName()).log(Level.INFO, method.toString());
						}

						webElement = context.invokeWait().waitForElementToLoadBy(by);

						try {
							return method.invoke(webElement, args);
						} catch (Exception e) {
							throw e.getCause();
						}

					}
				});
	}

}
