package edee.strings;

import java.util.Arrays;

import org.openqa.selenium.By;

import edee.commons.AppProperties;
import edee.strings.model.ElementData;
import edee.strings.model.MultiString;

/**
 * Class to access strings and elements data
 * 
 * @author Michal Travnicek
 *
 */
public class StringsSource {

	/**
	 * Simple locale descriptor: Languages available for test: CS-czech, EN-english.
	 *
	 */
	private String[] locales;

	private void init() {
		String properiesLocales = AppProperties.get(AppProperties.LOCALES).toLowerCase();
		locales = properiesLocales.split(",");
		for (String locale : locales) {
			if (!locale.matches("^[a-z]+$")) {
				throw new RuntimeException("Bad locale definition \"" + locale + "\" in properties."
						+ " Parameter \"locales\" must contain only comma separated locale identificators."
						+ " For example: cs, en.");
			}

		}
	}

	private DataSource dataSource;

	public StringsSource() {
		init();
		dataSource = new XMLDataSource();
	}

	/**
	 * Returns WebElement By selector from datasource of elements and strings
	 * 
	 * @param page
	 *            - page descriptor
	 * @param element
	 *            - element descriptor
	 * @return
	 */
	public By getByFromString(String page, String element) {

		ElementData result = null;
		try {
			result = dataSource.getElementDataByName(page, element);
		} 
		catch (Exception e) {
			throw new RuntimeException(e);
		}

		try {
			String selector = result.getSelector();
			String value = result.getValue();

			switch (selector) {
			case "className":
				return By.className(value);
			case "cssSelector":
				return By.cssSelector(value);
			case "id":
				return By.id(value);
			case "linkText":
				return By.linkText(value);
			case "name":
				return By.name(value);
			case "partialLinkText":
				return By.partialLinkText(value);
			case "tagName":
				return By.tagName(value);
			case "xPath":
				return By.xpath(value);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Returns string value from datasource of elements and strings
	 * 
	 * @param page
	 *            - page descriptor
	 * @param name
	 *            - string descriptor
	 * @return
	 */
	public String getStringValue(String page, String name) {

		String result = null;
		try {
			result = dataSource.getStringValue(page, name);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	/**
	 * Returns multistring value from datasource of elements and strings
	 * 
	 * @param page
	 *            - page descriptor
	 * @param name
	 *            - multistring descriptor
	 * @return
	 * @throws Exception
	 */
	public MultiString getMultiStringValue(String page, String name) {

		MultiString result = null;
		try {
			result = dataSource.getMultiStringValue(locales, page, name);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return result;

	}

	/**
	 * Retrieves string from multistring with desired locale.
	 * 
	 * @param multistring
	 * @param locale
	 * @return
	 */
	public String getLocaleString(MultiString multistring, String locale) {
		String result = multistring.getString(Arrays.asList(locales).indexOf(locale));
		return result;
	}

}
