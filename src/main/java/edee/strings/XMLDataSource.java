package edee.strings;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edee.commons.AppProperties;
import edee.strings.model.ElementData;
import edee.strings.model.MultiString;
import edee.strings.model.XmlDataModel;

/**
 * Implementation of DataSource using XML file placed in Resources directory
 * 
 * @author Michal Travnicek
 */
public class XMLDataSource extends XmlDataModel implements DataSource {

	private Document doc;
	private XPath xpath;

	public XMLDataSource() {
		
		String file = AppProperties.get(AppProperties.XML_PROPERTIES);

		try (InputStream in = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(file)) {
			if (in == null) {
				throw new FileNotFoundException(file);
			}

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			XPathFactory xPathfactory = XPathFactory.newInstance();
			xpath = xPathfactory.newXPath();

			doc = dBuilder.parse(in);
			doc.getDocumentElement().normalize();
			validateElements();
			in.close();

		} catch (Exception ex) {
			Logger.getLogger(StringsSource.class.getName()).log(Level.SEVERE, null, ex);
			throw new RuntimeException(ex);
		}

	}

	/**
	 * Simple element validation, TODO implement XML validation based on schema
	 */
	public void validateElements() {

		List<String> selectorList = Arrays.asList(ElementModel.SELECTORS);

		NodeList nList = doc.getElementsByTagName("element");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);

			Element eElement = (Element) nNode;
			String found = eElement.getAttribute("selector");
			if (!selectorList.contains(found)) {
				throw new RuntimeException("Bad selector found in database: " + found);
			}
		}
	}

	/*
	 * Reading element selector from datasource... critical asset.
	 */
	@Override
	public ElementData getElementDataByName(String page, String name) throws Exception {

		String subElement = ElementModel.DESCRIPTOR;
		String selectorQuery = "/@" + ElementModel.SELECTOR;
		String valueQuery = "/@" + ElementModel.VALUE;

		String query = getXpathExpression(page, subElement, name, selectorQuery);
		String selector = (String) xpath.compile(query).evaluate(doc, XPathConstants.STRING);

		String query2 = getXpathExpression(page, subElement, name, valueQuery);
		String value = (String) xpath.compile(query2).evaluate(doc, XPathConstants.STRING);

		if (selector.equals("") || value.equals("")) {

			throwNotFoundException("Element", page, name);
		} else {
			Logger.getLogger(getClass().getName()).log(Level.INFO,
					"element [" + page + "][" + name + "]" + "result:" + selector + " " + value);

		}

		return new ElementData(name, selector, value);

	}

	/**
	 * Reads MultiString from "multistring" element. We need MultiString to
	 * contain at least one string for example pageTitle etc.
	 *
	 * @param page
	 * @param name
	 * @return
	 */
	@Override
	public MultiString getMultiStringValue(String[] locales, String page, String name) throws Exception {

		String subElement = MultiStringModel.DESCRIPTOR;
		MultiString multistring = new MultiString();
		boolean found = false;

		for (int i = 0; i < locales.length; i++) {

			String expression = getXpathExpression(page, subElement, name, "/" + locales[i] + "/text()");

			Logger.getLogger(getClass().getName()).log(Level.FINE, "exp:" + expression.toString());

			XPathExpression expr = xpath.compile(expression);

			String result = (String) expr.evaluate(doc, XPathConstants.STRING);

			result = result.replaceAll("\\s+", " ");

			Logger.getLogger(getClass().getName()).log(Level.INFO,
					"multistring [" + page + "][" + name + "]" + "result:" + result);

			if (result.equals("")) {
				result = EMPTY_STRING;
			} else {
				found = true;
			}
			multistring.setString(i, result);
		}

		if (found == false) {
			throwNotFoundException("Multistring", page, name);
		}

		return multistring;

	}

	/**
	 * Reusable method for queries on page data.
	 * Pages can be nested, elements/values are searched on level of page node.
	 */
	private String getXpathExpression(String page, String subElement, String name, String query) {
		if (page == "" || subElement == "" || name == "" || query == "") {
			Logger.getLogger(getClass().getName()).log(Level.WARNING, "Invalid query: [page:" + page + "][element:"
					+ subElement + "][name:" + name + "][query:" + query + "]");
		}
		return "/" + ROOT + "//" + page + "/" + subElement + "[@name='" + name + "']" + query;
	}

	/*
	 * Reading string from datasource. Some strings can be noncritical for
	 * application so instead of exception we issue warning.
	 * 
	 */
	@Override
	public String getStringValue(String page, String name) throws Exception {

		String subElement = StringModel.DESCRIPTOR;

		String expression = getXpathExpression(page, subElement, name, "/@value");
		XPathExpression expr = xpath.compile(expression);
		String result = (String) expr.evaluate(doc, XPathConstants.STRING);

		if (result.equals("")) {
			Logger.getLogger(getClass().getName()).log(Level.WARNING,
					"String not found or empty!!! [" + page + "][" + name + "]");
		} else {
			Logger.getLogger(getClass().getName()).log(Level.INFO,
					"string [" + page + "][" + name + "]" + "result:" + result);
		}

		return result;

	}

	private void throwNotFoundException(String what, String page, String name) {
		throw new RuntimeException(what + " value not found in database: [page:" + page + "]" + "[name:" + name + "]");
	}

}
