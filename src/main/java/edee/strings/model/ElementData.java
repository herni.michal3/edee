package edee.strings.model;

/**
 * Container for fetching ElementData from datasource
 * @author Michal Travnicek
 *
 */
public class ElementData {

	/**
	 * Container for element data from datasource
	 * 
	 * @param name
	 * @param selector
	 * @param selectorValue
	 */
	public ElementData(String name, String selector, String selectorValue) {
		this.name = name;
		this.selector = selector;
		this.selectorValue = selectorValue;		
	}

	/**
	 * Descriptive name of element
	 */
	private final String name;

	/**
	 * By selector as String 
	 */
	private final String selector;
	
	/**
	 * Value used by selector 
	 */
	private final String selectorValue;

	public String getName() {
		return name;
	}
	
	public String getSelector() {
		return selector;
	}

	public String getValue() {
		return selectorValue;
	}


}
