package edee.strings.model;

public class XmlDataModel {

	protected static final String ROOT = "pages";
	
	protected XmlDataModel() {
		
	}

	protected static final class ElementModel {

		public static final String DESCRIPTOR = "element";
		public static final String NAME = "name";
		public static final String SELECTOR = "selector";
		public static final String VALUE = "value";
		
		public static final String[] SELECTORS = {"className", "cssSelector", "id", "linkText", "name", "partialLinkText", "tagName", "xPath"};
	}

	protected static final class MultiStringModel {

		public static final String DESCRIPTOR = "multistring";
		public static final String NAME = "name";

	}

	protected static final class StringModel {

		public static final String DESCRIPTOR = "string";
		public static final String NAME = "name";
		public static final String VALUE = "value";

	}

}
