package edee.strings.model;

import java.util.LinkedList;
import java.util.List;


public class MultiString {
	
	private final List<String> strings = new LinkedList<>();		
	
	
	public void setString(int index, String element) {
		strings.add(index, element);
	}	
	
	public String getString(int index) {
		return strings.get(index);
	}
	
	public int localesCount() {
		return strings.size();
        }

}
