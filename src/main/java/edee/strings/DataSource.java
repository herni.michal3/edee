/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edee.strings;

import edee.strings.model.ElementData;
import edee.strings.model.MultiString;

/**
 * Data source for retrieving strings and elements used in tests
 * @author Michal Travnicek
 */
public interface DataSource {
	
	public static final String EMPTY_STRING = "*EMPTY_STRING*";
		     
	/**
	 * Fetches element data from datasource. 
	 * 
	 * @param page - page descriptor
	 * @param name - descriptive name of element
	 * @return
	 */
	ElementData getElementDataByName(String page, String name) throws Exception;

	/**
	 * Fetches string value from datasource.
	 * 
	 * @param page - page descriptor
	 * @param name - descriptive name of string
	 * @return
	 */
	String getStringValue(String page, String name) throws Exception;

	/**
	 * Fetches multistring (string with localized variants) from datasource 
	 * 
	 * @param locales - locales to search in datasource - default is "en", "cs"
	 * @param page - page descriptor
	 * @param name - descriptive name of multistring
	 * @return
	 * @throws Exception
	 */
	MultiString getMultiStringValue(String[] locales, String page, String name) throws Exception;

}
