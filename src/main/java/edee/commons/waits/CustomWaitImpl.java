package edee.commons.waits;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import edee.commons.AppProperties;
import edee.commons.ApplicationContext;

public class CustomWaitImpl implements CustomWait {

	private static final long DEFAULT_SLEEP_MILLISECONDS = Long
			.valueOf(AppProperties.get(AppProperties.DEFAULT_SLEEP_MILLISECONDS));
	private static final long DEFAULT_TIMEOUT_SECONDS = Long
			.valueOf(AppProperties.get(AppProperties.DEFAULT_TIMEOUT_SECONDS));
	private WebDriverWait waitInstance;

	/**
	 * Invokes wait instance with default timeout and sleep
	 * @param context
	 */
	public CustomWaitImpl(ApplicationContext context) {
		this(context, DEFAULT_TIMEOUT_SECONDS, DEFAULT_SLEEP_MILLISECONDS);
	}

	/**
	 * Invokes wait instance with default sleep and defined timeout
	 * @param context
	 * @param waitTimeout
	 */
	public CustomWaitImpl(ApplicationContext context, long waitTimeout) {
		this(context, waitTimeout, DEFAULT_SLEEP_MILLISECONDS);
	}

	/**
	 * Invokes wait instance with defined timeout and sleep
	 * @param context
	 * @param waitTimeout
	 * @param waitSleep
	 */
	public CustomWaitImpl(ApplicationContext context, long waitTimeout, long waitSleep) {
		waitInstance = new WebDriverWait(context.getDriver(), waitTimeout, waitSleep);
	}

	@Override
	public void waitForJavaScriptDOMReady() {
		waitInstance.until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
				.executeScript("return document.readyState").equals("complete"));
	}

	@Override
	public boolean waitFor(final ExpectedCondition<?>... conditions) {
		return waitInstance.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.or(conditions));
	}

	@Override
	public boolean waitForElementText(WebElement element, String text) throws Exception {
		return waitInstance.until(ExpectedConditions.textToBePresentInElement(element, text));
	}

	@Override
	public WebElement waitForElementToAppear(WebElement element) throws Exception {
		return waitInstance.until(ExpectedConditions.visibilityOf(element));
	}

	@Override
	public WebElement waitForElementToBeClickable(WebElement element) throws Exception {
		return waitInstance.until(ExpectedConditions.elementToBeClickable(element));
	}

	@Override
	public boolean waitForURL(String fragment) throws Exception {
		return waitInstance.until(ExpectedConditions.urlContains(fragment));
	}

	@Override
	public boolean waitForURLMatch(String regex) throws Exception {
		return waitInstance.until(ExpectedConditions.urlMatches(regex));
	}

	@Override
	public WebElement waitForElementToLoadBy(By bye) throws Exception {
		return waitInstance.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.presenceOfElementLocated(bye));
	}

}
