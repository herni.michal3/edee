package edee.commons.waits;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import edee.strings.model.MultiString;

/**
 * Extension of ExpectedConditions offered by Selenium.
 *
 * @author Michal Travnicek
 *
 */
public class CustomExpectedConditions {

	/**
	 * Page title is basic if crude method to identify page loaded. This condition
	 * returns true if the page title equals "title".
	 *
	 * @param title
	 * @return
	 */
	public static ExpectedCondition<Boolean> pageTitleis(final String title) {

		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {

				return driver.getTitle().equals(title);
			}
		};
	}

	/**
	 * This condition returns true if the page title equals "title".
	 * Modified version for multistrings (localized strings)
	 *
	 * @param title
	 * @return
	 */
	public static ExpectedCondition<Boolean> pageTitleis(final MultiString title) {

		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {

				for (int i = 0; i < title.localesCount(); i++) {
					if (title.getString(i).equals(driver.getTitle())) {
						return true;
					}
				}

				return false;
			}
		};
	}
	

	/**
	 * Check that current page has different title than "title"
	 *
	 * @param title
	 * @return
	 */
	public static ExpectedCondition<Boolean> pageTitleisNot(final String title) {

		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {

				return !driver.getTitle().equals(title);
			}
		};
	}

	/**
	 * Check that current page has different title than "title". Version for multistring.
	 * @param title
	 * @return
	 */
	public static ExpectedCondition<Boolean> pageTitleisNot(final MultiString title) {

		return new ExpectedCondition<Boolean>() {

			String searchedTitle;
			String actualTitle;

			@Override
			public Boolean apply(WebDriver driver) {
				actualTitle = driver.getTitle();
				for (int i = 0; i < title.localesCount(); i++) {
					searchedTitle = title.getString(i);
					if (searchedTitle.equals(actualTitle)) {
						return false;
					}
				}

				return true;
			}

			@Override
			public String toString() {
				return String.format("text to NOT match pattern \"%s\". Current text: \"%s\"", searchedTitle,
						actualTitle);
			}

		};
	}

	/**
	 * Matcher for text pattern in element using regex from multistring
	 *
	 * @param element
	 * @param regex
	 * @return
	 */
	public static ExpectedCondition<Boolean> textMatchesPattern(final WebElement element, final MultiString regex) {
		return new ExpectedCondition<Boolean>() {
			private String currentValue = null;
			private Pattern pattern = null;

			@Override
			public Boolean apply(WebDriver driver) {
				currentValue = element.getText();
				for (int i = 0; i < regex.localesCount(); i++) {
					try {
						pattern = Pattern.compile(regex.getString(i));
						if (pattern.matcher(currentValue).find()) {
							Logger.getLogger(getClass().getName()).log(Level.INFO,
									"Pattern match:" + pattern.pattern());
							return true;
						}

					} catch (Exception e) {
						return false;
					}
				}
				return false;
			}

			@Override
			public String toString() {
				return String.format("text to match pattern \"%s\". Current text: \"%s\"", pattern.pattern(),
						currentValue);
			}
		};
	}

}
