package edee.commons.waits;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomWaitProxy {
	
	private static final String MATCH_METHODS = "wait.*";

	CustomWait object;

	public CustomWait getWaitProxy(CustomWait object) {
		this.object = object;
		return (CustomWait) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { CustomWait.class },
				new WaitInvocationHandler());
	}

	private class WaitInvocationHandler implements InvocationHandler {

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

			if (method.getName().matches(MATCH_METHODS)) {

				String argument = " ";
				try {
					argument += (String) args[0];
				} catch (Exception e) {
				}

				Logger.getLogger(getClass().getName()).log(Level.INFO, method.getName() + argument);

			}
			try {
				return method.invoke(object, args);
			} catch (Exception e) {
				throw e.getCause();
			}

		}

	}
}
