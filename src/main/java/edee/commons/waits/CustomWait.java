package edee.commons.waits;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

public interface CustomWait {

	/**
	 * Waits for DOM Ready state.
	 */
	void waitForJavaScriptDOMReady();
	
	/**
	 * Shortcut for ExpectedConditions.or
	 * @param conditions
	 * @return
	 */
	boolean waitFor(ExpectedCondition<?>... conditions);

	/**
	 * Waits for element to contain specific text.
	 * @param element
	 * @param text
	 * @return
	 * @throws Exception
	 */
	boolean waitForElementText(WebElement element, String text) throws Exception;

	/**
	 * Waits for element using condition ExpectedConditions.visibilityOf.
	 * @param element
	 * @return
	 * @throws Exception
	 */
	WebElement waitForElementToAppear(WebElement element) throws Exception;

	/**
	 * Waits for element using condition ExpectedConditions.elementToBeClickable.
	 * @param element
	 * @return
	 * @throws Exception
	 */
	WebElement waitForElementToBeClickable(WebElement element) throws Exception;	

	/**
	 * Waits for URL to contain specific text.
	 * @param URLfragment
	 * @return
	 * @throws Exception
	 */
	boolean waitForURL(String URLfragment) throws Exception;	

	/**
	 * Waits for element to load using defined By locator.
	 * @param bye - By locator
	 * @return 
	 * @throws Exception
	 */
	WebElement waitForElementToLoadBy(By bye) throws Exception;

	/**
	 * Waits for URL to match regex.
	 * @param URLregex
	 * @return
	 * @throws Exception
	 */
	boolean waitForURLMatch(String URLregex) throws Exception;

}