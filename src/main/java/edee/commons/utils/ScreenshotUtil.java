package edee.commons.utils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.TakesScreenshot;

import edee.commons.AppProperties;

public class ScreenshotUtil {
	
	private static final String PATH = AppProperties.get(AppProperties.SCREENSHOT_FOLDER) + File.separator;
	private static final String IMAGE_EXTENSION = ".png";
	
	/**
	 * Creates screenshot with path: SCREENSHOT_FOLDER + name + IMAGE_EXTENSION.
	 *  
	 * @param driverContext
	 * @param name - name with path
	 * @throws Exception
	 */
	public static void screenshotToSetFolder(SearchContext driverContext, String name) throws Exception {
		takeScreenshot(driverContext, PATH + name + IMAGE_EXTENSION);
	}
	
	
	/**
	 * Creates screenshot with set name/path.
	 * @param driverContext
	 * @param name - name with path
	 * @throws Exception
	 */
	public static void takeScreenshot(SearchContext driverContext, String name) throws Exception {
		System.out.println(new File(name).getAbsolutePath());

		TakesScreenshot screen = (TakesScreenshot) driverContext;
		File file = screen.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file, new File(name));
	}

}
