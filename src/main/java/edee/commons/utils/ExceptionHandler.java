package edee.commons.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.NoSuchElementException;

public class ExceptionHandler {

	public static void notFound(Class<?> source, Throwable e, String what) {

		if (e.getClass().equals(NoSuchElementException.class)) {
			Logger.getLogger(source.getName()).log(Level.INFO, what + " does not exist! " + source);
		} else {
			Logger.getLogger(source.getName()).log(Level.WARNING,
					"Unknown error while getting " + what.toLowerCase() + ". source:" + source);
		}
	}
	
	public static void failed(Class<?> source, Throwable e, String what) throws Throwable {
			Logger.getLogger(source.getName()).log(Level.INFO, what + " failed! " + source);
            throw e;
	}

}
