package edee.commons.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PageUtils {
		
	/**
	 * Extracts string from text based on pattern.
	 * @param regex
	 * @param text
	 * @return
	 */
	public static String extractPattern(String regex, String text) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		if (matcher.find()) {
		  return matcher.group(1);
		}
		return "";
	}	

	/**
	 * Method to convert URL to pattern... used in experiments for loading pages by pattern.
	 * Not reliable. For example firefox/geckodriver gives in URL %22 instead of char ".
	 *
	 * 
	 * @param url
	 * @return
	 */
	public static String patternizeURL(String url) {

		String result = url;
		result = result.replaceAll("\\?", "\\\\?");
		result = result.replaceAll("\\[", "\\\\[");
		result = result.replaceAll("\\]", "\\\\]");
		result = result.replaceAll("\\}", "\\\\}");
		result = result.replaceAll("\\{", "\\\\{");
		result = result.replaceAll("lang=..", "lang=.*");
		result = result.replaceAll("qf/../edeeui/", "qf/.*/edeeui/");
		if (url.contains("%22")) {
			result = result.replaceAll("\"", "%22");
		}
		System.out.println("url:" + url);
		System.out.println("mod:" + result);

		return result;
	}

}
