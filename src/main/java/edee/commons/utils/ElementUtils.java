package edee.commons.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ElementUtils {
	
	/**
	 * Origin element to start lookup from.
	 */
	private WebElement element;
	
	public ElementUtils (WebElement element) {
		this.element = element;		
	}
	
	/**
	 * Returns element's parent element.
	 * @return
	 */
	public WebElement getParent() {		
		return element.findElement(By.xpath(".."));
	}
	
	/**
	 * Returns element's before element.
	 * @return
	 */
	public WebElement getBeforeSibling() {		
		return element.findElement(By.xpath("preceding-sibling::*"));
	}
	
	/**
	 * Returns element's after element.
	 * @return
	 */
	public WebElement getAfterSibling() {		
		return element.findElement(By.xpath("following-sibling::*"));
	}
	
	/**
	 * Returns elements containing text (not exact match).
	 * Use ".//" to limit search to the children of this WebElement (see findElementsBy)
	 * @param text
	 * @return
	 */
	public List<WebElement> getElementsWithText(String text) {
		return element.findElements(By.xpath("//*[contains(text(), '" + text + "')]"));
	}

}
