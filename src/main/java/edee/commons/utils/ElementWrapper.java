package edee.commons.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebElement;

import edee.commons.ApplicationContext;

/**
 * Wrapper useful for catching exception on WebElement etc.
 * 
 * @author Michal T
 *
 */
public class ElementWrapper {

	private WebElement element;

	public ElementWrapper(WebElement element) {
		this.element = element;
	}

	public final WebElement getElement(ApplicationContext context) {
		return (WebElement) Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[] { WebElement.class },
				new InvocationHandler() {

					@Override
					public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

						if (element == null) {
							throw new RuntimeException(
									"Called \"" + method.getName() + "\" on null element (not found?).");
						}

						if (method.toString().matches(".*WebElement.*")) {						
							Logger.getLogger(getClass().getName()).log(Level.INFO, method.toString());
						}

						try {
							return method.invoke(element, args);
						} catch (Exception e) {
							throw e.getCause();
						}

					}
				});
	}

}
