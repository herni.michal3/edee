package edee.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import edee.commons.driver.ChromeDriverConfig;
import edee.commons.driver.WebDriverConfig;
import edee.commons.waits.CustomWait;
import edee.commons.waits.CustomWaitImpl;
import edee.commons.waits.CustomWaitProxy;
import edee.strings.StringsSource;

/**
 * WebDriver context containing WebDriver instance and support functions
 *
 * @author Michal Travnicek
 *
 */
public class ApplicationContext {
	
	
	/**
	 * Interim setup of basic java.util logger.
	 *
	 */
    static {
        String path = ApplicationContext.class.getClassLoader().getResource("logging.properties").getFile();
        System.setProperty("java.util.logging.config.file", path);
    }

    private WebDriver webDriver;
    private final WebDriverConfig DEFAULT_CONFIG;
    private final CustomWaitProxy waitProxy;
    private final StringsSource strings;
    private Set<Cookie> sessionStore;

    public ApplicationContext() {
        waitProxy = new CustomWaitProxy();
        strings = new StringsSource();
        DEFAULT_CONFIG = new ChromeDriverConfig();
    }

    public ApplicationContext(WebDriverConfig config) {
        this();
        setDriver(config);
    }

    /**
     * Returns WebDriver instance
     *
     * @return
     */
    public WebDriver getDriver() {
        return webDriver;
    }

    /**
     * Create WebDriver instance from config Chrome, Firefox etc...
     *
     * @param config
     */
    public void setDriver(WebDriverConfig config) {
        webDriver = config.getDriver();
        System.out.println("STARTING NEW DRIVER INSTANCE:" + config.toString());
    }

    /**
     * Saves all cookies in session for reuse
     */
    public void saveSession() {
        sessionStore = webDriver.manage().getCookies();
    }

    /**
     * Saves session to file
     * @throws IOException
     */
    public void saveSessionToFile() throws IOException {
        ObjectOutput out = new ObjectOutputStream(new FileOutputStream("cookie.file"));
        out.writeObject(sessionStore);
        out.close();
    }

    /**
     * Loads cookies from file.
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
	public void loadSessionFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        File file = new File("cookie.file");
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        sessionStore = (Set<Cookie>) in.readObject();
        in.close();
    }

    /**
     * Restores all saved cookies in session
     */
    public void restoreSession() {
        for (Object c : sessionStore) {
            Cookie cuk = (Cookie) c;
            System.out.println(cuk);
            webDriver.manage().addCookie(cuk);
        }
    }

    /**
     * Reuses driver instance and only erase cookie session. Can be modified that instead new WebDriver instance is returned.
     */
    public void getNewSession() {
        webDriver.manage().deleteAllCookies();

    }

    /**
     * Returns accessor method for strings datasource
     *
     * @return
     */
    public StringsSource strings() {
        return strings;
    }

    /**
     * Invokes custom wait with default timeout and sleep
     *
     * @return
     */
    public CustomWait invokeWait() {
        return waitProxy.getWaitProxy(new CustomWaitImpl(this));
    }

    /**
     * Invokes custom wait with defined "timeout"
     *
     * @param timeout
     * @return
     */
    public CustomWait invokeWait(int timeout) {
        return waitProxy.getWaitProxy(new CustomWaitImpl(this, timeout));
    }

    /**
     * Invokes custom wait with defined "timeout" and "sleep" (refresh interval)
     *
     * @param timeout
     * @param sleep
     * @return
     */
    public CustomWait invokeWait(int timeout, int sleep) {
        return waitProxy.getWaitProxy(new CustomWaitImpl(this, timeout, sleep));
    }

    /**
     * Quits webdriver.
     */
    public void quitWebDriver() {
        webDriver.quit();

    }

    /**
     * Initialize WebDriver with default config.
     */
    public void initializeWebDriver() {
        setDriver(DEFAULT_CONFIG);
    }

    /**
     * Initialize WebDriver with supplied config.
     *
     * @param config
     */
    public void initializeWebDriver(WebDriverConfig config) {
        setDriver(config);
    }

}
