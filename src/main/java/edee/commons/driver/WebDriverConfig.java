/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edee.commons.driver;

import org.openqa.selenium.WebDriver;

/**
 * Used for getting instance of WebDriver - we can select multiple implementations of Chrome, Firefox, etc.. 
 * @author Michal Travnicek
 *
 */
public interface WebDriverConfig {

    /**
     * Returns instance of WebDriver 
     * @return
     */
    WebDriver getDriver();
    
}
