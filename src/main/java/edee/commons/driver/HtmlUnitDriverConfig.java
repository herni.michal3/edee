package edee.commons.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * Deprecated HtmlUnit driver for experimenting, not fully compatible with "real" Chrome or Firefox
 * @author Michal Travnicek
 *
 */
public class HtmlUnitDriverConfig implements WebDriverConfig {
	
    
    @Override
    public WebDriver getDriver() {
		return new HtmlUnitDriver();
	}

}
