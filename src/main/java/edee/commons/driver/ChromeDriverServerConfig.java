package edee.commons.driver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import edee.commons.AppProperties;

/**
 * In server mode chromedriver(.exe) must be started from commandline.
 * It launches server which offers fast startup of tests.
 * 
 * @author Michal Travnicek
 *
 */
public class ChromeDriverServerConfig implements WebDriverConfig {

	private static final String SERVER_URL = AppProperties.get(AppProperties.CHROMEDRIVER_SERVER_URL);
	private static final String BINARY = "binary";

	/**
	 * For connecting to chromedriver launched from commandline.
	 * 
	 * @return
	 */
	@Override
	public WebDriver getDriver() {

		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		String binaryPath = AppProperties.get(AppProperties.CHROME_PATH);
		if (binaryPath != null) {
			chromeOptions.put(BINARY, binaryPath);
		}
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		try {
			return new RemoteWebDriver(new URL(SERVER_URL), capabilities);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
