package edee.commons.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import edee.commons.AppProperties;

public class FirefoxDriverConfig implements WebDriverConfig {	

    /**
     * Firefox driver and browser config for Linux and Windows
     * @return
     */
    @Override
    public WebDriver getDriver() {

    	FirefoxOptions options = new FirefoxOptions();
    	options.setBinary(AppProperties.get(AppProperties.GECKO_PATH));
		return new FirefoxDriver(options);
	}

}
