package edee.commons.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import edee.commons.AppProperties;

public class ChromeDriverConfig extends AppProperties implements WebDriverConfig {

    /**
     * Chromedriver and Chrome/Chromium config - for Linux and Windows.
     * @return
     */
    @Override
    public WebDriver getDriver() {
	      
		ChromeOptions options = new ChromeOptions();
		String binaryPath = AppProperties.get(CHROME_PATH);
		if (binaryPath!=null) {
			options.setBinary(binaryPath);
		}		
		AppProperties.setSystemProperty(CHROMEDRIVER_PATH);
		AppProperties.setSystemProperty(WEBDRIVER_CHROME_VERBOSE_LOGGING);

	    return new ChromeDriver(options);
		
	}

}
