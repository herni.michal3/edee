package edee.commons;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * Loads application properties from file located in resources folder.
 * 
 * @author Michal T.
 *
 */
public class AppProperties {

	public static final String CLASSPATH = System.getProperty("user.dir");
	public static final String LOCALES = "LOCALES";
	public static final String XML_PROPERTIES = "XML_PROPERTIES";
	public static final String SCREENSHOT_FOLDER = "SCREENSHOT_FOLDER";
	public static final String UPLOAD_FILE = "UPLOAD_FILE";
	
	public static final String DEFAULT_SLEEP_MILLISECONDS = "DEFAULT_SLEEP_MILLISECONDS";
	public static final String DEFAULT_TIMEOUT_SECONDS = "DEFAULT_TIMEOUT_SECONDS";

	private static final String LINUX_GECKO_PATH = "LINUX_GECKO_PATH";
	private static final String WIN_GECKO_PATH = "WIN_GECKO_PATH";
	private static final String LINUX_CHROME_PATH = "LINUX_CHROME_PATH";
	private static final String WIN_CHROME_PATH = "WIN_CHROME_PATH";
	private static final String LINUX_GECKODRIVER_PATH = "LINUX_GECKODRIVER_PATH";
	private static final String WIN_GECKODRIVER_PATH = "WIN_GECKODRIVER_PATH";
	private static final String LINUX_CHROMEDRIVER_PATH = "LINUX_CHROMEDRIVER_PATH";
	private static final String WIN_CHROMEDRIVER_PATH = "WIN_CHROMEDRIVER_PATH";
	
	public static final String CHROMEDRIVER_SERVER_URL = "CHROMEDRIVER_SERVER_URL";

	private static final int OS_ID_LINUX = 1;
	private static final int OS_ID_WINDOWS = 0;
	private static Properties propertiesCache;
	private static List<String> requiredProperties;

	private static final String OS_NAME_PROPERTY = "os.name";
	private static final String APPLICATION_PROPERTIES_FILE = "application.properties";
	private static final String WINDOWS_DESCRIPTOR = "windows";

	/**
	 * Path to browser - optional, if not set default install location is used.
	 */
	public static final String CHROME_PATH = "webdriver.chrome.path";
	public static final String GECKO_PATH = "webdriver.gecko.path";

	/**
	 * This is preset property name used by chromedriver - must be set
	 */
	public static final String CHROMEDRIVER_PATH = "webdriver.chrome.driver";

	/**
	 * This is preset property name used by geckodriver (Firefox) - must be set
	 */
	public static final String GECKODRIVER_PATH = "webdriver.gecko.driver";
	public static final String WEBDRIVER_CHROME_VERBOSE_LOGGING = "webdriver.chrome.verboseLogging";

	static {

		String path = ApplicationContext.class.getClassLoader().getResource(APPLICATION_PROPERTIES_FILE).getFile();

		try (InputStream is = new FileInputStream(path)) {
			propertiesCache = new Properties();
			propertiesCache.load(is);
		} catch (Exception e) {
			new RuntimeException(e);
		}

		requiredProperties = new ArrayList<String>();

		requiredProperties.addAll(Arrays.asList(new String[] 
		{LOCALES, XML_PROPERTIES, SCREENSHOT_FOLDER, DEFAULT_SLEEP_MILLISECONDS, DEFAULT_TIMEOUT_SECONDS, CHROMEDRIVER_SERVER_URL}));

		for (String s : requiredProperties) {
			if (checkEmpty(propertiesCache.getProperty(s))) {
				Logger.getLogger(AppProperties.class.getName()).log(Level.WARNING, "Required property not found " + s);
			}
		}

		requiredProperties.add(CHROMEDRIVER_PATH);
		requiredProperties.add(GECKODRIVER_PATH);

		int osSelector = osSelector();

		selectProperty(CHROMEDRIVER_PATH, new String[] { WIN_CHROMEDRIVER_PATH, LINUX_CHROMEDRIVER_PATH }, osSelector);
		selectProperty(GECKODRIVER_PATH, new String[] { WIN_GECKODRIVER_PATH, LINUX_GECKODRIVER_PATH }, osSelector);

		selectProperty(CHROME_PATH, new String[] { WIN_CHROME_PATH, LINUX_CHROME_PATH }, osSelector);
		selectProperty(GECKO_PATH, new String[] { WIN_GECKO_PATH, LINUX_GECKO_PATH }, osSelector);

	}

	/**
	 * Reads and caches property based on selector, for example by detected os.
	 * Properties are not set as system properties only saved to properties cache.
	 * Nonexistent and zero sized properties are considered as not set.
	 * 
	 * @param propertyToSet
	 *            - property to be saved
	 * @param variants
	 *            - array of variants
	 * @param selector
	 *            - numeric selector
	 * @param required
	 *            - is property critical?
	 */
	public static void selectProperty(String propertyToSet, String[] variants, int selector) {

		String readProperty = propertiesCache.getProperty(variants[selector]);

		if (!checkEmpty(readProperty)) {
			propertiesCache.setProperty(propertyToSet, readProperty);
		} else {
			if (requiredProperties.contains(propertyToSet)) {
				Logger.getLogger(AppProperties.class.getName()).log(Level.WARNING,
						"Required property not found \"" + propertyToSet + "\" " + variants[selector]);
			}
		}

	}

	private static boolean checkEmpty(String value) {
		return value == null || value.length() < 1;
	}

	/**
	 * Sets cached property as system property.
	 * 
	 * @param key
	 */
	public static void setSystemProperty(String key) {

		String value = get(key);

		if (checkEmpty(value)) {
			Logger.getLogger(AppProperties.class.getName()).log(Level.WARNING,
					"Undefined property not set \"" + key + "\" " + value);
		} else {
			System.setProperty(key, value);
		}
	}

	/**
	 * Get cached property for string with access check.
	 * 
	 * @param key
	 * @return
	 */
	public static String get(String key) {

		String value = propertiesCache.getProperty(key);

		if (requiredProperties.contains(key) && checkEmpty(value)) {
			throw new RuntimeException("Attempted to use required property that is not set: " + key);
		}

		return value;
	}

	/**
	 * Detects OS. For the sake of demo we consider other system than Windows as
	 * Linux.
	 * 
	 * @return 0/1
	 */
	public static int osSelector() {

		String os = System.getProperty(OS_NAME_PROPERTY).toLowerCase();
		Logger.getLogger(AppProperties.class.getName()).log(Level.INFO,
				"Detected operating system:\"" + os.toUpperCase() + "\"");

		if (os.contains(WINDOWS_DESCRIPTOR)) {
			return OS_ID_WINDOWS;
		} else
			return OS_ID_LINUX;

	}

}
