/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edee.workers;

import edee.commons.ApplicationContext;

/**
 * Worker class to execute simple operation
 * @author Michal Travnicek
 */
public interface Worker {

    void execute(ApplicationContext context) throws Exception;
    
}
