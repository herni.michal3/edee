package edee.workers;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import edee.commons.ApplicationContext;

public class UploadRobot implements Worker {

	private static final int WAIT_BEFORE = 4000;
	private static final int WAIT_AFTER = 4000;
	
    private String filePath;
    
    
    /**
     * 
     * Interim uploader implemented by Robot, need to wait for safety (about 4 seconds)
     * for Upload dialog to appear then wait some more after file is being submitted and uploaded.
     */
    public UploadRobot() {

	}
    
    /**
     * Path to file to upload.
     * @param filePath
     * @return
     */
    public UploadRobot setFilePath (String filePath) {	
    	this.filePath = filePath;
    	return this;
    }
    
    
    /**
     * Copies String (path) to clipboard. Works both on Windows and Linux. 
     * @param string
     */
    private void setClipboardData(String string) {
        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
    }

    /* 
     * Dialog of upload opens at drive root or Documents etc.
     * We need to set clipboard to location of file to upload...
     */
    @Override
    public void execute(ApplicationContext context) throws Exception {
    	
    	System.out.println("Waiting for upload dialog to show");
    	Thread.sleep(WAIT_BEFORE);
    	setClipboardData(filePath);
    	robotKeySequence();  	   
    	System.out.println("Waiting for upload to finish");
    	Thread.sleep(WAIT_AFTER);  	

    }
    
    /**
     * 
     * Paste string from clipboard into upload dialog via CTRL+V shortcut and ENTER to submit.
     * 
     * @throws AWTException
     */
    private void robotKeySequence() throws AWTException {

        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);	
  
    }

}
