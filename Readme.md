﻿# Edee test suite demo

## Design considerations:
* Use Selenium based Java tests for testing web application available in multiple languages (currently CS and EN version).
* Application uses cookie session storage for access management.
* Sessions (cookies) cannot be restored.
* Solution aims for reusability and scalability.
* We may need storage for data used by tests.
* Page Object pattern is encouraged.

## Implementation:

##### Page Objects
Page Object pattern is in Selenium implemented by PageFactory which uses encapsulated WebElements retrieved using FindBy annotations.

https://github.com/SeleniumHQ/selenium/wiki/PageFactory

Due to nature of annotations in general they have to contain strings/parameters that cannot be externalized even within the same class.
For example:
```
@FindBy(how = How.NAME, using = "q")
WebElement elementName;
```
That is why I propose using custom annotations that are not using parameters at all....
```
@FindByName
WebElement elementName;
```
These annotations depend on data storage which looks up element name in database and retrieves corresponding selector, string etc.
So we need storage for data used in tests: web element descriptors, URLs, string data from webpages.

See:
http://www.seleniumhq.org/docs/06_test_design_considerations.jsp#user-interface-mapping

Because Java properties cannot store multiple values, XML storage was chosen as simple data storage.
Implementation uses interface which permits to switch to other kind of datastore like SQL database etc.
Used XML file is structured so that top level represents all pages, first depth is a page, then data itself as string, multistring or element. Elements/strings can be even nested.
Page Object class uses own proxy which injects corresponding string, multistring or element by name from data storage.

Because of advanced capabilities over JUnit, TestNG framework is used.
Due to usage of cookies in webapp and for performance reasons, WebDriver instance reuse is proposed. Every next test session can be reset by clearing cookies (or reloading them).
This behavior can easily be changed. Tests based on TestNG can natively run multiple times and in the same time it is possible to run tests in parallel on arbitrary number of threads (browser instances), this is implemented and tested in code while it can be further extended by using server solution like Selenium Grid etc. Multiple different browsers can be used – Firefox, Chromium etc...

Whole application without login page needs authentication, so test has to pass login prompt every time or reuse WebDriver instance with session loaded.

#### Code is structured in following way:

* WebContext is passed around containing WebDriver instance with Custom Wait and Util classes.
* DataStorage interface is accessed via ElementStrings and implemented as XML storage.
* Tests are based on PageObject classes with nesting as soft dependency.

While using separate Scenario classes was considered, this would lead to bigger code fragmentation. So tests are contained in @Test methods for now and using Worker classes (reusable steps). (can be changed later).

TestNG natively supports running testcases based on XML defined steps....this is possible enhancement, not implemented/tried yet.

File uploader function uses some flash applet which I cannot control and must use Robot worker to simulate user interacting with window. (Does it work in Linux too?)

#### Further notes/TODO:
* There is issue with Chromium browser, which closes itself after 30 seconds with WebDriver no matter what. This is nonexistent in Chrome.
* Using of headless Chrome should be considered for performance reasons (HTML unit driver is not useful). But because Chrome offers headless capability only in Linux and development is done mainly on Windows, this is not tested yet.
* Screenshot based validation will be implemented later.
<br>see: http://elementalselenium.com/tips/62-web-consistency-testing
<br>see: https://screenster.io/pricing/
* Allure reporting – TODO

#### How to run tests:
* IDE with support for TestNG and Maven based project.
* Tests are preset to use chromedriver server for fast execution.
* Start chromedriver exe from classpath of project and then run tests from IDE.
* Or change test setup to ChromeDriverConfig - chromedriver in standard mode etc.
* There is one xml testsuite class UploadAnDelete.xml that should be runnable as TestNG test.
* For more info see http://testng.org/doc/documentation-main.html#running-testng





